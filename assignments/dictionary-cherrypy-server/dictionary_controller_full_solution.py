# class lecture code for cherrypy - day 1
import cherrypy
import re, json

class DictionaryController(object):
    def __init__(self):
        self.myd = dict()

    def get_value(self, key):
        return self.myd[key]


    # event handlers for resource requests
    def GET_KEY(self, key):
        output = {'result':'success'} #first create default output
        key = str(key) # check/convert input to correct format

        try:
            value = self.get_value(key)
            if value is not None:
                output['key']   = key
                output['value'] = value
            else:
                output['result']    = 'error'
                output['message']   = 'none type value associated with requested key'
        except KeyError as ex:
            output['result']    = 'error'
            output['message']   = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output) #returns string version of json output


    def GET_INDEX(self):
    #when get request is received without an mid, we return all entries in the dictionaryself.
        output = {'result':'success'}
        output['entries'] = list()
        #take all the entries from self.myd and add them to output so that we can send them in the response.

        try:
            for key in self.myd.keys():
                val = self.myd[key]
                d_entry = {'key': key, 'value' : val}
                output['entries'].append(d_entry)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def PUT_KEY(self, key):
        output = {'result': 'success'}
        key = str(key)
        #get body of message into data
        data = json.loads(cherrypy.request.body.read())

        try:
            val = data['value']
            self.myd[key] = val #storing the value of 'value' in val
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)


    def DELETE_KEY(self, key):
        output = {'result': 'success'}
        key = str(key)

        try:
            del self.myd[key]
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)


    def DELETE_INDEX(self):
        output = {'result': 'success'}
        self.myd = dict()
        return json.dumps(output)


    def POST_INDEX(self):
        output = {'result': 'success'}
        data = json.loads(cherrypy.request.body.read())

        try:
            key = data['key']
            val = data['value']
            self.myd[key] = val
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)
