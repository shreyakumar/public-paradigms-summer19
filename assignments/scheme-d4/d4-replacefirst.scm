;; scheme homework 1 - daily 4
;; name: <your name here>
;; date: <the date here>

;; the test lists
;; notice they have a ' in front of the lists; that tells the interpreter to read
;; the list literally (e.g., as atoms, instead of functions)
(define testlist1 '(turkey gravy stuffing yams ham peas))
(define testlist2 '(bacon turkey beef turkey))
(define testlist3 '(bacon turkey beef))

;; replacefirst
(define replacefirst
  (lambda (a b lat)
    '() ;; currently returns an empty list, TODO: your solution here
  )
)
    ;; right now replacefirst always returns an empty list
    ;; make it return a lat with the first instance of a replaced with b
    ;; see the tests and example output below...

;; tests to check your output. Do not change these tests.
(display (replacefirst 'turkey 'cheese testlist1))
(display "\n")

(display (replacefirst 'turkey 'bacon testlist2))
(display "\n")

(display (replacefirst 'sauce 'apple testlist3))
(display "\n")

;; notice that this is equivalent to using testlist3
(display (replacefirst 'beef 'carrots (list 'bacon 'turkey 'beef)))
(display "\n")

;; correct output:
;;   $ guile d4-replacefirst.scm
;;   (cheese gravy stuffing yams ham peas)
;;   (bacon bacon beef turkey)
;;   (bacon turkey beef)
;;   (bacon turkey carrots)
