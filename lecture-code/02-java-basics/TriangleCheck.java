import java.util.Scanner;

public class TriangleCheck { 
    public static void main(String[] args) {
        System.out.println("Enter upper bound: ");
        Scanner in = new Scanner(System.in);
        int upperBound = in.nextInt();
        printValidTriangles(upperBound);
        System.out.println("Using classes:\n");
        Triangle.printValidTriangles(upperBound);
    }

    public static void printValidTriangles(int upperBound){
    for (int c = 1; c <= upperBound; c++) { 
        for (int b = 1; b < c; b++) {
            for (int a = 1; a < b; a++){
                if (a * a + b * b == c * c) {
                    System.out.printf("%d %d %d\n", a, b, c);
                }
            }
        }
    }
   }
}

class Triangle{
    int a, b, c;

    Triangle(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public boolean valid(){
        if( (a*a + b*b == c*c) || (a*a + c*c == b*b) || (b*b + c*c == a*a) )
            return true;
        else
            return false;
    }

    public void printTriangle(){
        System.out.printf("%d %d %d\n", this.a, this.b, this.c);
    }

    public static void printValidTriangles(int upperBound){
        for(int a = 1; a <= upperBound; a++){
            for(int b = 1; b < a; b++){
                for(int c = 1; c < b; c++){
                    Triangle temp = new Triangle(a, b, c);
                    if(temp.valid()){
                        temp.printTriangle();    
                    }    
                }
            }
        }
    } 
}

            
