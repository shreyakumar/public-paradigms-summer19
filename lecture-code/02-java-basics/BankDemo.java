class Account{

    String acNum;
    String customerName;

    Account(String acNum, String customerName){
        this.acNum = acNum;
        this.customerName = customerName;
    }

    String getAcNum(){ return this.acNum; }
    void setAcNum(String acNum){ this.acNum = acNum; }

    String getCustName() { return this.customerName; }
    void setCustName(String customerName) { this.customerName = customerName; }
}

class Checking extends Account{

    int withdrawalLimit;

    Checking(String acNum, String customerName, int withdrawalLimit){
        super(acNum, customerName);
        this.withdrawalLimit = withdrawalLimit;
    }

    double getWLimit(){ return this.withdrawalLimit; }
    void setWLimit(int limit) { this.withdrawalLimit = limit; } 

}

class Savings extends Account{
    double interestRate;

    Savings(String acNum, String customerName, double interestRate){
        super(acNum, customerName);
        this.interestRate = interestRate;
    }

    double getIntRate() { return this.interestRate; }
    void setIntRate(double rate) { this.interestRate = rate; }

}

public class BankDemo{ // this class only exists to have a main method that uses the classes in the inheritance heirarchy and to give the name BankDemo.java to this file
    public static void main(String[] args){
        Checking ac1 = new Checking("A123", "Dwight Schrute", 10000);
        System.out.println(ac1.getAcNum() + " created for " + ac1.getCustName() + " has a limit of " +  ac1.getWLimit());

    }

}
