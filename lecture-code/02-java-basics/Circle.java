public class Circle {
    private double x; 
    private double y; 
    private double r;

    public Circle(double x, double y, double r) { 
        this.x = x;
        this.y = y; 
        this.r = r;
    } 
    
    public static Circle create(double x, double y, double r) {
        return new Circle(x, y, r);
    } 
    
    public double area() {
        return Math.PI * this.r * this.r;
    }
    
    public static double area(double r) { 
        return Math.PI * r * r;
    }

    public static void main(String[] args){
        Circle c1 = new Circle(5, 6, 3);
        System.out.println("The area is " + c1.area());
    }
}
