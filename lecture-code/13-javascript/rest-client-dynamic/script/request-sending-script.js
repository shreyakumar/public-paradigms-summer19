console.log("started script!"); // for debugging

// add reaction to Send button
send_button = document.getElementById("send-button"); // get button
send_button.onmouseup = makeRequest; // associate button with function.

// react to checkbox use-key
var key_input_added = false;
var key_checkbox = document.getElementById("checkbox-use-key");
key_checkbox.addEventListener('change', function() {
    if(this.checked && (key_input_added == false)) { // first time
        // only create the key once
        createKeyElementsDynamically();
        key_input_added = true;
    } else if (this.checked){ // when key is checked after first time
        toggleVisibility("input-key-dynamic");
        toggleVisibility("label-key-dynamic");
    }
    else { // when key is unchecked
        // hideKeyElementsDynamically
        toggleVisibility("input-key-dynamic");
        toggleVisibility("label-key-dynamic");
    }
});

// working with dynamic elements

function createKeyElementsDynamically(){
    var keyLabel = new Label();
    keyLabel.createLabel("Enter key(dynamic):", "label-key-dynamic");
    keyLabel.addAtPositionById("div-for-key");

    var inputBox = new InputText();
    inputBox.createInput("input-key-dynamic");
    inputBox.addAtPositionById("div-for-key");
} // end of createKeyElementsDynamically

function toggleVisibility(id){
    var someItem = document.getElementById(id);
    if (someItem.style.display === "none") {
        someItem.style.display = "block"; // hide
    } else {
        someItem.style.display = "none"; // show
    }
} // end of toggleVisibility

// dealing with form values

function getValuesFromForm(){
    //select dropdown select-server-address
    var selidx = document.getElementById('select-server-address').selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selidx].value;
    console.log("url_base=" + url_base);
    // text input-port-number
    var port_num = document.getElementById('input-port-number').value;
    console.log("port number: " + port_num);
    //radio group - button1 id="radio-get", value="GET" id="radio-put"

    var action = "GET"; // default
    if (document.getElementById('radio-get').checked){
        action = "GET";
    } else if (document.getElementById('radio-put').checked) {
        action = "PUT";
    } else if (document.getElementById('radio-post').checked) {
        action = "POST";
    } else if (document.getElementById('radio-delete').checked){
        action = "DELETE";
    }

    var key = null;
    if (document.getElementById('checkbox-use-key').checked){
        // get key value
        key = document.getElementById('input-key-dynamic').value;
        console.log("key: " + key);

    }
    // get url together
    var url = url_base + ":" + port_num + "/movies/";
    if (key != null){
        url = url + key;
    }

    var message_body = null;
    if (document.getElementById('checkbox-use-message').checked){
        // get key value
        message_body = document.getElementById('text-message-body').value;
    }

    var parameters = [action, url, message_body];
    console.log(parameters);
    return parameters;
} // end of get Values from Form

// doing network stuff

function makeRequest(){
    // get values from form elements
    console.log("started make request!");
    // get networking data ready
    var url = "http://student04.cse.nd.edu:51002/movies/32";
    // make network call

    var params = getValuesFromForm();

	var xhr = new XMLHttpRequest();
    xhr.open(params[0], params[1], true);

	xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('response-label').innerHTML = xhr.responseText;
                var movie_json = JSON.parse(xhr.responseText);
                var title = movie_json['title'];
                var genres = movie_json['genres'];
                document.getElementById('answer-label').innerHTML = title + " belongs to the genres: " + genres;

			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}
	xhr.send(params[2]);
} // end of makeRequest
