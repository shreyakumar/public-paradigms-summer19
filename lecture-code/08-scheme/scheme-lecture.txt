Scheme lecture 01
------
(define p '(1 2 3))
(define p '(1 2 3 hello #t () bye))

(car p) produces 1
(cdr p) produces (2 3 hello #t () bye)

(cadr p) produces 2, car of cdr of p

(length p) produces 7

(cons 1 (cons 2 '(3))) produces (1 2 3)
(cons 'e '(1)) produces (e 1)

(car (cdr p))       same as (cadr p))
2

(car (cdr (cdr p)))     same as (caddr p)
3

----------- conditionals ---------

(cond
((eq? 1 1) 'hello)
(#t        'goodbye))

--------- writing OR-----------

(define or
(lambda (a b)
    		(cond
    			(a #t)
    			(b #t)
    			(else #f)
)
    	) ; end of lambda function
)

guile> (or 0 0)
#t
guile> (or 0 1)
#t
guile> (or #f #f)
#f

---------- writing OR list -------
(define or_list
 (lambda (l)
   (cond
    	((null? l) #f)
	    ((car l)   #t)
    	(else (or_list (cdr l)))
    )
   ) ; end of lambda function
)

(display (or_list '(#f #f #f #t)))
#t
(display (or_list '(#f #f #f #f)))
#f
