package com.example.shreyakumar.l01_android_summer20;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import android.util.Log;
import android.widget.Toast;

// importing our special new package
import com.example.shreyakumar.l01_android_summer20.utilities.NetworkUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;


public class MainActivity extends AppCompatActivity {

    private TextView mSearchResultsDisplay;
    private EditText mSearchTermEditText;
    private Button mSearchButton;
    private Button mResetButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // R class which statically allows access to non compiling entities.

        // connecting to the visual elements
        mSearchResultsDisplay   = (TextView) findViewById(R.id.tv_display_text); // explicit cast
        mSearchTermEditText     = (EditText) findViewById(R.id.et_search_box);
        mSearchButton           = (Button) findViewById(R.id.search_button);
        mResetButton            = (Button) findViewById(R.id.reset_button);

        // populating names in text view
        mSearchResultsDisplay.append("Dwight\n\nHarry");

        final String[] studentNames = {"Matthew", "Rose", "Jack", "Claire", "Aram", "Andrew", "Sydney", "Chris", "Frank", "Annie"};

        for(String name: studentNames){
            mSearchResultsDisplay.append("\n\n" + name);
        } // end of for

        final String defaultDisplayText = mSearchResultsDisplay.getText().toString(); // use this string to reset text

        // perform Search - detecting and responding to clicks
        mSearchButton.setOnClickListener(
                new View.OnClickListener() { // an unnamed object passed to setOnClickListener

                    public void onClick(View v){ // inner method def
                        // get search string
                        String searchText = mSearchTermEditText.getText().toString();
                        Log.d("informational", "Search button clicked with text: " + searchText);
                        System.out.println("Search button clicked with text: " + searchText);

                        // create Toast
                        Context c = MainActivity.this; // this!!!
                        Toast.makeText(c, "Search clicked!", Toast.LENGTH_LONG).show(); // static method

                        //another way to perform search instead
                        makeNetworkSearchQuery();

                        /*

                        // perform search
                        for(String name : studentNames){
                            Log.d("informational", "Checking name: " + name);
                            if(name.toLowerCase().equals(searchText.toLowerCase())){
                                mSearchResultsDisplay.setText(name);
                                break;
                            }
                            else{
                                mSearchResultsDisplay.setText("No result found!");
                            }
                        } // end for

                        */



                    } // end of inner function - onClickView

                } // end of View.OnClickListener
        ); // end of setOnClickListener


        // perform reset of text - detecting and responding to clicks
        mResetButton.setOnClickListener(
                new View.OnClickListener() { // an unnamed object passed to setOnClickListener

                    public void onClick(View v){ // inner method def
                        // get search string
                        mSearchResultsDisplay.setText(defaultDisplayText);

                    } // end of inner function - onClickView

                } // end of View.OnClickListener
        ); // end of setOnClickListener

    } // end of onCreate method

    /* Networking begins */

    public void makeNetworkSearchQuery(){
        // get search string
        String searchTerm = mSearchTermEditText.getText().toString();
        // reset search results
        mSearchResultsDisplay.setText("Results for " + searchTerm + ":\n\n");
        // make network query
        new FetchNetworkData().execute(searchTerm);
    } // end of makeNetworkSearchQuery

    // inner Networking Async class
    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params){ // this is called when execute runs
            if(params.length == 0) return null;
            String searchTerm = params[0];

            //URL searchUrl = NetworkUtilities.buildRedditUrl(searchTerm);
            URL searchUrl = NetworkUtilities.buildCountriesUrl();
            //perform networking task
            String responseString = null;
            try {
                responseString = NetworkUtilities.getResponseFromUrl(searchUrl);
                Log.d("informational", responseString);
            } catch (Exception e){
                e.printStackTrace();
            }
            return responseString;
        } // end of doInBackground

        @Override
        protected void onPostExecute(String responseData){
            // this is invoked when the network thread finishes its networking call.
            //String [] titles = NetworkUtilities.parseRedditJson(responseData);
            String [] titles = NetworkUtilities.parseCountriesJson(responseData);
            // display news titles in GUI
            for (String title: titles){
                mSearchResultsDisplay.append("\n\n" + title);
            } // end for

        } // end of onPostExecute


    } // end of inner class FetchNetworkData

    /* Networking ends */



    /* Menu related functions below */

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    } // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.menu_about){
            Log.d("informational", "About menu item selected!");

            Class destinationActivity = AboutActivity.class;

            // create Intent to invoke new Activity
            Intent startAboutActivityIntent = new Intent(MainActivity.this, destinationActivity);
            // add extra msg to the intent(optional)
            //String msg = "Some special message";
            String msg = mSearchTermEditText.getText().toString();
            startAboutActivityIntent.putExtra(Intent.EXTRA_TEXT, msg);

            startActivity(startAboutActivityIntent);
            Log.d("informational", "About activity launched!");
        }
        return true;
    } // end of onOptionsItemSelected

} // end of Main activity
