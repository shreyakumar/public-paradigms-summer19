package com.example.shreyakumar.l01_android_summer20.utilities;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by shreyakumar on 6/24/20.
 */

public class NetworkUtilities {


    public static URL buildRedditUrl(String searchTerm){
        final String baseUrl = "https://www.reddit.com/r/";
        final String endformat = ".json";
        URL redditUrl = null;
        String urlString = baseUrl +  searchTerm + endformat;
        try{
            redditUrl = new URL(urlString);
            Log.d("informational", "URL:" + urlString);
        } catch(MalformedURLException e){
            System.out.println("The url is not correctly formatted.");
            e.printStackTrace();
        }
        return redditUrl;
    } // end buildUrl

    public static String[] parseRedditJson(String redditResponseString){
        String[] newsTitles = new String[25];
        try{
            JSONObject allNewsReddit = new JSONObject(redditResponseString);
            JSONObject allNewsObject = allNewsReddit.getJSONObject("data");
            JSONArray children = allNewsObject.getJSONArray("children");
            newsTitles = new String[children.length()];
            for (int i = 0; i < newsTitles.length; i++){
                JSONObject childJson = children.getJSONObject(i);
                JSONObject childData = childJson.getJSONObject("data");
                String title = childData.getString("title");

                newsTitles[i] = title;
            }   // end of for
        }catch(JSONException e){
            e.printStackTrace();
        }
        return newsTitles;
    }  // end of parseRedditJson



    public static URL buildCountriesUrl(){
        String countryUrlString = "https://api.openaq.org/v1/countries";
        URL countryUrl = null;
        try{
            countryUrl = new URL(countryUrlString);
            Log.d("informational", "URL:" + countryUrlString);
        } catch(MalformedURLException e){
            System.out.println("The url is not correctly formatted.");
            e.printStackTrace();
        }
        return countryUrl;
    } // end buildCountriesUrl

    public static String[] parseCountriesJson(String countriesResponseString){
        String[] countryList = new String[100];
        try{
            JSONObject allCountriesObject = new JSONObject(countriesResponseString);
            JSONArray allCountriesArray = allCountriesObject.getJSONArray("results");
            countryList = new String[allCountriesArray.length()];
            Log.d("info", "Length: "+ allCountriesArray.length());
            for (int i = 0; i < allCountriesArray.length(); i++){
                JSONObject childJson = allCountriesArray.getJSONObject(i);
                if (childJson.has("name")) { // check if that entry has key "name"
                    String title = childJson.getString("name");
                    Log.d("info", title);
                    countryList[i] = title;
                }
            }   // end of for
        }catch(JSONException e){
            e.printStackTrace();
        }
        return countryList;
    }  // end of parseRedditJson



    public static String getResponseFromUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try{
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A"); // this is an end of message delimiter
            boolean hasInput = scanner.hasNext();
            if (hasInput) return scanner.next();
            else return null;
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        } // end of finally
        return null;
    } // end of getResponseFromUrl




} // end of class NetworkUtilities
