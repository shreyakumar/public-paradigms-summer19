package com.example.shreyakumar.lecture01_android_summer20;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    private TextView mDisplayAboutTextView;
    private Button  mOpenWebpageButton;
    private Button mOpenMapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // connect to GUI elements
        mDisplayAboutTextView = (TextView) findViewById(R.id.tv_about_text);
        mOpenMapButton = (Button) findViewById(R.id.button_open_map);
        mOpenWebpageButton = (Button) findViewById(R.id.button_open_webpage);

        final String urlString = "https://www.nd.edu/";

        //get information that previous page sent
        mDisplayAboutTextView.setText("About page!");
        String message = "hello";
        Intent intentThatStartedThisActivity = getIntent();
        // check if extra message is there
        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            message = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mDisplayAboutTextView.append("\n\n" + message);
        }


        // open webpage button
        mOpenWebpageButton.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                            Log.d("info", urlString);
                        openWebPage(urlString); //write this function
                    } // end of onClick
                } // end of View.OnClickListener
        ); // end of setOnClickListener

        // open map button
        mOpenMapButton.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        openMap(); //write this function
                    } // end of onClick
                } // end of View.OnClickListener
        ); // end of setOnClickListener

    } // end of onCreate

    public void openMap(){
        String addressString = "University of Notre Dame, IN";
        Uri addressUri = Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", addressString).build();
        Log.d("info", "addressUri: " + addressUri.toString());

        Intent openMapIntent = new Intent(Intent.ACTION_VIEW);
        openMapIntent.setData(addressUri);

        //check if I can run this Intent
        if(openMapIntent.resolveActivity(getPackageManager()) != null){
            startActivity(openMapIntent);
        }
    } // end of openMap

    public void openWebPage(String urlString){
        Uri webpage = Uri.parse(urlString);

        Intent openWebpageIntent = new Intent(Intent.ACTION_VIEW, webpage);
        //check if that intent can be launched
        if(openWebpageIntent.resolveActivity(getPackageManager()) != null){
            startActivity(openWebpageIntent);
        } // end if
    } // end of open webpage


} // end of AboutActivity
