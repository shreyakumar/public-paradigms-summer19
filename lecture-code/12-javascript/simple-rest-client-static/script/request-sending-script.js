console.log("started script!");
// get button
send_button = document.getElementById("send-button");
// associate button with function.
send_button.onmouseup = makeRequest;

function getValuesFromForm(){
    //select dropdown select-server-address
    var selidx = document.getElementById('select-server-address').selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selidx].value;
    console.log("url_base=" + url_base);
    // text input-port-number
    var port_num = document.getElementById('input-port-number').value;
    console.log("port number: " + port_num);
    //radio group - button1 id="radio-get", value="GET" id="radio-put"

    var action = "GET"; // default
    if (document.getElementById('radio-get').checked){
        action = "GET";
    } else if (document.getElementById('radio-put').checked) {
        action = "PUT";
    } else if (document.getElementById('radio-post').checked) {
        action = "POST";
    } else if (document.getElementById('radio-delete').checked){
        action = "DELETE";
    }

    var key = null;
    if (document.getElementById('checkbox-use-key').checked){
        // get key value
        key = document.getElementById('input-key').value;
        console.log("key: " + key);

    }
    // get url together
    var url = url_base + ":" + port_num + "/movies/";
    if (key != null){
        url = url + key;
    }

    var message_body = null;
    if (document.getElementById('checkbox-use-message').checked){
        // get key value
        message_body = document.getElementById('text-message-body').value;
    }

    var parameters = [action, url, message_body];
    console.log(parameters);
    return parameters;
} // end of get Values from Form

function makeRequest(){
    // get values from form elements
    console.log("started make request!");
    // get networking data ready
    var url = "http://student04.cse.nd.edu:51002/movies/32";
    // make network call

    var params = getValuesFromForm();

	var xhr = new XMLHttpRequest();
    xhr.open(params[0], params[1], true);

	xhr.onload = function(e) {
			if (xhr.readyState === 4) {
				console.log(xhr.responseText);
                document.getElementById('response-label').innerHTML = xhr.responseText;
                var movie_json = JSON.parse(xhr.responseText);
                var title = movie_json['title'];
                var genres = movie_json['genres'];
                document.getElementById('answer-label').innerHTML = title + " belongs to the genres: " + genres;

			} else {
				console.error(xhr.statusText);
			}
		};

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}


	xhr.send(params[2]);
} // end of makeRequest
